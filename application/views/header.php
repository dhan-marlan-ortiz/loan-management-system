<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Loan Management System</title>

    <link href= "<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href= "<?php echo base_url('assets/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link href= "<?php echo base_url('assets/sass/css/styles.css') ?>" rel="stylesheet">


    <script src="<?php echo base_url('assets/bootstrap/js/jquery.min.js'); ?> "></script>
    <script src="<?php echo base_url('assets/bootstrap/js/jquery.validate.min.js'); ?> "></script>

  </head>
  <body style="background-image: url(<?=base_url('assets/images/circles.png')?>);">
    

    
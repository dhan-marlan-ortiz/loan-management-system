<br> 

<div class='row'>
	<div class='col-sm-offset-4 col-sm-4'>
		<div class="panel panel-default">
			<div class="panel-heading">
				<image src="<?=base_url('assets/images/emsys_logo.png')?>" class='img=responsive center-block'>
			</div>
			<div class="panel-body">

				<form id="loginForm" method="POST" action="landing/login_authentication" autocomplete="off">
					<h4 class='page-header text-center'>PLEASE SIGN IN TO CONTINUE<h4>
					<div class="input-group">
						<span class="input-group-addon"><i class='fa fa-user-o fa-fw '></i></span>
						<input type="text" class="form-control " placeholder="USERNAME *"  id="uname" name="uname" >
					</div>
					<small id="uname_errmsg" class="text-danger text-uppercase"></small>
					<br>
					<div class="input-group">
						<span class="input-group-addon"><i class='fa fa-lock fa-fw '></i></span>
						<input type="password" class="form-control" placeholder="PASSWORD *" id="pw" name="pw">
					</div>
					<small id="pw_errmsg" class="text-danger text-uppercase"></small>
					<br>
					<p class='text-center'>
						<strong>
							<small id='note-required'>(*) REQUIRED FIELDS</small>
						</strong>
					</p>
					<button class='btn btn-primary btn-block' type="submit"><i class='fa fa-sign-in fa-fw '></i> SIGN IN</button>
				</form>
			</div>
		</div>

		<p class='text-center copyright'><small>&copy DHAN MARLAN A. ORTIZ | 2017</small></p>
	</div>
</div>



<script>

	$(function validate() {

	    var rules = {
	        rules: {
	            uname: {
	                minlength: 2,
	                maxlength: 20,
	                required: true
	            },
	            pw: {
	            	maxlength: 30,
	            	required: true
	            }
	        },
	        errorPlacement: function (error, element) {
	            var name = $(element).attr("name");
	            error.appendTo($("#" + name + "_errmsg"));
	        },
	    };

	    $('#loginForm').validate(rules);

	});

</script>	